// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "MSALWrapper",
  platforms: [
    .iOS(.v13)
  ],
  products: [
    .library(
      name: "MSALWrapper",
      targets: ["MSAL"]),
  ],
  dependencies: [
  ],
  targets: [
    .target(
      name: "MSALWrapper",
      dependencies: []),
    .binaryTarget(
      name: "MSAL",
      path: "MSAL.xcframework"
    ),
    .testTarget(
      name: "MSALWrapperTests",
      dependencies: ["MSALWrapper"]),
  ]
)
